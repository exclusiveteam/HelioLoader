
client.notify("INOSTRANEC Ez Owned")

-- SNOW [NEW]
local background_alpha = 0
local snowflake_alpha = 0

local screen = engine.get_screen_size()

local new_checkbox = ui.add_check_box

local background_enabled = new_checkbox('background', 'snowflake_background', true)
local in_game = new_checkbox('show in game', 'snowflake_ingame', false)

local function clamp(min, max, val)
    if val > max then return max end
    if val < min then return min end
    return val
end

local function draw_line(x, y, x1, y1, r, g, b, a)
    renderer.line(vec2_t.new(x, y), vec2_t.new(x1, y1), color_t.new(r, g, b, a))
end

local function draw_rect(x, y, w, h, r, g, b, a)
    renderer.rect_filled(vec2_t.new(x, y), vec2_t.new(x + w, y + h), color_t.new(r, g, b, a))
end

local function draw_snowflake(x, y, size)
    local base = 4 + size

    draw_line(x - base, y - base, x + base + 1, y + base + 1, 255, 255, 255, snowflake_alpha - 75)
    draw_line(x + base, y - base, x - base, y + base, 255, 255, 255, snowflake_alpha - 75)

    base = 5 + size

    draw_line(x - base, y, x + base + 1, y, 255, 255, 255, snowflake_alpha - 75)
    draw_line(x, y - base, x, y + base + 1, 255, 255, 255, snowflake_alpha - 75)
end

local snowflakes = {}
local time = 0
local stored_time = 0

local function on_render()
    local show_in_game = in_game:get_value()

    if background_enabled:get_value() then
        if ui.is_visible() and background_alpha ~= 255 then
            background_alpha = clamp(0, 255, background_alpha + 10)
            snowflake_alpha = clamp(0, 255, snowflake_alpha + 10)
        end

        if not ui.is_visible() and background_alpha ~= 0 then
            background_alpha = clamp(0, 255, background_alpha - 10)
            snowflake_alpha = clamp(0, 255, snowflake_alpha - 10)
        end

        if ui.is_visible() or background_alpha ~= 0 then
            draw_rect(0, 0, screen.x, screen.y, 0, 0, 0, background_alpha - 90)
        end
    end

    if not show_in_game and not ui.is_visible() then
        return
    end

    if show_in_game then
        snowflake_alpha = 255
    end

    local frametime = globalvars.get_frame_time()

    time = time + frametime

    if #snowflakes < 128 then
        if time > stored_time then
            stored_time = time

            table.insert(snowflakes, {
                math.random(10, screen.x - 10),
                1,
                math.random(1, 3),
                math.random(-60, 60) / 100,
                math.random(-3, 0)
            })
        end
    end

    local fps = 1 / frametime

    for i = 1, #snowflakes do
        local snowflake = snowflakes[i]
        local x, y, vspeed, hspeed, size = snowflake[1], snowflake[2], snowflake[3], snowflake[4], snowflake[5]

        if screen.y <= y then
            snowflake[1] = math.random(10, screen.x - 10)
            snowflake[2] = 1
            snowflake[3] = math.random(1, 3)
            snowflake[4] = math.random(-60, 60) / 100
            snowflake[5] = math.random(-3, 0)
        end

        draw_snowflake(x, y, size)

        snowflake[2] = snowflake[2] + vspeed / fps * 100
        snowflake[1] = snowflake[1] + hspeed / fps * 100
    end
end

client.register_callback('paint', on_render)


-- Buy Bot
client.register_callback("round_start", function(event)
    is_round_started = true
end)

client.register_callback("round_prestart", function(event)
    is_round_started = true
end)




client.register_callback("create_move", function(cmd)
	
	if is_round_started then
		buy_bot( )
		is_round_started = false
	end

end)

is_round_started = false

pistols_list = {
	["0"] = "",
	["1"] = "buy glock; buy hkp2000; buy usp_silencer;",
	["2"] = "buy elite;",
	["3"] = "buy p250;",
	["4"] = "buy tec9; buy fiveseven;",
	["5"] = "buy deagle; buy revolver;",
}

pistols_name_list = {

	"None",
	"Glock-18/HKP2000/USP-S",
	"Dual Berretas",
	"P250",
	"Tec-9/Five7",
	"Deagle/Revolver"

}

weapons_list = {
	["0"] = "",
	["1"] = "buy ssg08;",
	["2"] = "buy awp;",
	["3"] = "buy scar20; buy g3sg1;",
	["4"] = "buy galilar; buy famas;",
	["5"] = "buy ak47; buy m4a1; buy m4a1_silencer;",
	["6"] = "buy sg556; buy aug;",
	["7"] = "buy nova;",
	["8"] = "buy xm1014;",
	["9"] = "buy mag7;",
	["10"] = "buy m249;",
	["11"] = "buy negev;",
	["12"] = "buy mac10; buy mp9;",
	["13"] = "buy mp7;",
	["14"] = "buy ump45;",
	["15"] = "buy p90;",
	["16"] = "buy bizon;"
}

weapons_name_list = {

	"None",
	"SSG08",
	"AWP",
	"Scar20/G3SG1",
	"GalilAR/Famas",
	"AK-47/M4A1",
	"AUG/SG556",
	"Nova",
	"XM1014",
	"Mag-7",
	"M249",
	"Negev",
	"Mac-10/MP9",
	"MP7",
	"UMP-45",
	"P90",
	"Bizon"

}

other_list = {
	["0"] = "buy vesthelm;",
	["1"] = "buy hegrenade;",
	["2"] = "buy molotov; buy incgrenade;",
	["3"] = "buy smokegrenade;",
	["4"] = "buy taser;",
	["5"] = "buy defuser;"
}

other_name_list = {

	"Armor",
	"HE",
	"Molotov/Incgrenade",
	"Smoke",
	"Taser",
	"Defuser"

}

function buy_bot( )

	local pistol = pistols_list[tostring(buy_pistol:get_value(""))]
	local weapon = weapons_list[tostring(buy_weapon:get_value(""))]
	local other  = ""

	for i = 0, 5 do
		other = other..(buy_other:get_value(i) and other_list[tostring(i)] or "")
	end

	engine.execute_client_cmd(pistol)
	engine.execute_client_cmd(weapon)
	engine.execute_client_cmd(other)

end

buy_pistol = ui.add_combo_box("Pistol", "_pistols", pistols_name_list, 0)
buy_weapon = ui.add_combo_box("Weapon", "_weapons", weapons_name_list, 0)
buy_other = ui.add_multi_combo_box("Other", "_other", other_name_list, { false, false, false, false, false, false })


-- Fast Weapon After Nade
client.register_callback('grenade_thrown', function(i)
    if engine.get_player_for_user_id(i:get_int('userid', 0)) == entitylist.get_local_player():get_index() then
        engine.execute_client_cmd('slot2')
        engine.execute_client_cmd('slot1')
    end
end)


-- Fps Boost
local function print_fps()
    se.get_convar("r_3dsky"):set_int(0)
    se.get_convar("r_shadows"):set_int(0)
    se.get_convar("cl_csm_static_prop_shadows"):set_int(0)
    se.get_convar("cl_csm_shadows"):set_int(0)
    se.get_convar("cl_csm_world_shadows"):set_int(0)
    se.get_convar("cl_foot_contact_shadows"):set_int(0)
    se.get_convar("cl_csm_viewmodel_shadows"):set_int(0)
    se.get_convar("cl_csm_rope_shadows"):set_int(0)
    se.get_convar("cl_csm_sprite_shadows"):set_int(0)
    se.get_convar("cl_disablefreezecam"):set_int(1)
    se.get_convar("cl_freezecampanel_position_dynamic"):set_int(0)
    se.get_convar("cl_freezecameffects_showholiday"):set_int(0)
    se.get_convar("cl_showhelp"):set_int(0)
    se.get_convar("cl_autohelp"):set_int(0)
    se.get_convar("cl_disablehtmlmotd"):set_int(1)
    se.get_convar("mat_postprocess_enable"):set_int(0)
    se.get_convar("fog_enable_water_fog"):set_int(0)
    se.get_convar("gameinstructor_enable"):set_int(0)
    se.get_convar("cl_csm_world_shadows_in_viewmodelcascade"):set_int(0)
    se.get_convar("cl_disable_ragdolls"):set_int(1)
end

client.register_callback("paint", print_fps)

local function on_unload()
    se.get_convar("r_3dsky"):set_int(1)
    se.get_convar("cl_csm_static_prop_shadows"):set_int(1)
    se.get_convar("cl_csm_shadows"):set_int(1)
    se.get_convar("cl_csm_world_shadows"):set_int(1)
    se.get_convar("cl_foot_contact_shadows"):set_int(0)
    se.get_convar("cl_csm_viewmodel_shadows"):set_int(1)
    se.get_convar("cl_csm_rope_shadows"):set_int(1)
    se.get_convar("cl_csm_sprite_shadows"):set_int(1)
    se.get_convar("cl_disablefreezecam"):set_int(1)
    se.get_convar("cl_freezecampanel_position_dynamic"):set_int(0)
    se.get_convar("cl_freezecameffects_showholiday"):set_int(0)
    se.get_convar("mat_postprocess_enable"):set_int(1)
    se.get_convar("fog_enable_water_fog"):set_int(1)
    se.get_convar("cl_csm_world_shadows_in_viewmodelcascade"):set_int(1)
    se.get_convar("cl_disable_ragdolls"):set_int(0)
end

client.register_callback("unload", on_unload)


-- Grenade Esp
local verdana_def = renderer.setup_font( "C:/windows/fonts/verdana.ttf", 13, 0 )
local verdana_bold = renderer.setup_font( "C:/windows/fonts/verdana.ttf", 30, 32 )

local origin = se.get_netvar( "DT_BaseEntity", "m_vecOrigin" )
local vec_view = se.get_netvar( "DT_BasePlayer", "m_vecViewOffset[0]" )
local tick = se.get_netvar( "DT_BaseCSGrenadeProjectile", "m_nExplodeEffectTickBegin" )

math.round = function( num, idp )
  local mult = 10^( idp or 0 )
  return math.floor( num * mult + 0.5 ) / mult
end

local function distance_in_ft( o, dest )
	local yo = vec3_t.new( dest.x - o.x, dest.y - o.y, dest.z - o.z )
	return math.round( math.sqrt( yo.x * yo.x + yo.y * yo.y + yo.z * yo.z ) / 12, 3 )
end

local function arc( x, y, r1, r2, s, d, col )
	local i = s
	
	while i < s + d do
		i = i + 1
		
		local m_rad = i * math.pi / 180
		renderer.line( vec2_t.new( x + math.cos( m_rad ) * r1, y + math.sin( m_rad ) * r1 ), vec2_t.new( x + math.cos( m_rad ) * r2, y + math.sin( m_rad ) * r2 ), col )
	
	end
end

local function call_molotov_fire( pos )
	local get_int = 150
	local eye_pos = pos
	
	local grenades1 = entitylist.get_entities_by_class_id( 100 )
	local correct_shit = 0
	
	if grenades1 then
		for i = 1, #grenades1 do
		
			local grenade = grenades1[ i ]
			
			if grenade then
			
				local ent_origin = grenade:get_prop_vector( origin )
				local m_tick = grenade:get_prop_float( 0x20 )
		
				local n_fact = ( m_tick + 7 - globalvars.get_current_time( ) ) / 7
				local pos2d = se.world_to_screen( ent_origin )
		
				local fraction, hit_entity_index = trace.line( engine.get_local_player( ), 33570827, eye_pos, ent_origin )
		
				local dist = distance_in_ft( eye_pos, ent_origin )
				local safe = dist > 5 or fraction < 0.61
	
				if dist > 99 then
					correct_shit = 4
				else
					correct_shit = 0
				end
		
				if dist < get_int then
			
					if safe then
						renderer.circle( vec2_t.new( pos2d.x, pos2d.y - 50 ), 30, 30, true, color_t.new( 20, 20, 20, 175) )
					else
						renderer.circle( vec2_t.new( pos2d.x, pos2d.y - 50 ), 30, 30, true, color_t.new( 225, 20, 20, 175) )
					end
			
					renderer.text( "!", verdana_bold, vec2_t.new( pos2d.x - 5, pos2d.y - 75 ), 32, color_t.new( 255, 250, 175, 200 ) )
					renderer.text( tostring( math.round( dist, 0 ) ) .. " ft", verdana_def, vec2_t.new( pos2d.x - 13 - correct_shit, pos2d.y - 43 ), 13, color_t.new( 255, 255, 255, 200 ) )
			
					arc( pos2d.x, pos2d.y - 50, 30, 32, -90, 360 * n_fact, color_t.new( 232, 232, 232, 200 ) );
				end
			
			end
		
		end
	end
	
	
end

local function call_molotov( pos )
	local get_int = 150	
	local eye_pos = pos
	
	local grenades = entitylist.get_entities_by_class("CMolotovProjectile")
	local correct_shit = 0
	
	if grenades then
		for i = 1, #grenades do
		
			local grenade = grenades[ i ]
			
			if grenade then
				
				local ent_origin = grenade:get_prop_vector( origin )
		
				local pos2d = se.world_to_screen( ent_origin )
				local fraction, hit_entity_index = trace.line( engine.get_local_player( ), 33570827, eye_pos, ent_origin )
		
				local dist = distance_in_ft( eye_pos, ent_origin )
				local safe = dist > 5 or fraction < 0.61
		
				if dist > 99 then
					correct_shit = 4
				else
					correct_shit = 0
				end
		
				if dist < get_int then
			
					if safe then
						renderer.circle( vec2_t.new( pos2d.x, pos2d.y - 50 ), 30, 30, true, color_t.new( 20, 20, 20, 175) )
					else
						renderer.circle( vec2_t.new( pos2d.x, pos2d.y - 50 ), 30, 30, true, color_t.new( 225, 20, 20, 175) )
					end
			
					renderer.text( "!", verdana_bold, vec2_t.new( pos2d.x - 5, pos2d.y - 75 ), 32, color_t.new( 255, 250, 175, 200) )
					renderer.text( tostring( math.round( dist, 0) ) .. " ft", verdana_def, vec2_t.new( pos2d.x - 13 - correct_shit, pos2d.y - 43 ), 13, color_t.new( 255, 255, 255, 200 ) )
			
					
						arc( pos2d.x, pos2d.y - 50, 30, 32, -90, 360, color_t.new( 232, 232, 232, 200 ) );
					
					
				end
			end
		end	
	end
end

local function call_he( pos )
	local get_int = 150
	local eye_pos = pos
	
	local grenades = entitylist.get_entities_by_class_id( 9 )
	local correct_shit = 0
	
	if grenades then
		for i = 1, #grenades do
		
			local grenade = grenades[ i ]
			
			if grenade then
			
				local mm_tick = grenade:get_prop_int( tick )
				local ent_origin = grenade:get_prop_vector( origin )
				local pos2d = se.world_to_screen( ent_origin )
			
				local fraction, hit_entity_index = trace.line( engine.get_local_player( ), 33570827, eye_pos, ent_origin )
		
				local dist = distance_in_ft( eye_pos, ent_origin )
				local safe = dist > 5 or fraction < 0.61
		
				if dist > 99 then
					correct_shit = 4
				else
					correct_shit = 0
				end
		
				if dist < get_int and mm_tick == 0 then
			
					if safe then
						renderer.circle( vec2_t.new( pos2d.x, pos2d.y - 50 ), 30, 30, true, color_t.new( 20, 20, 20, 175 ) )
					else
						renderer.circle( vec2_t.new( pos2d.x, pos2d.y - 50 ), 30, 30, true, color_t.new( 225, 20, 20, 175 ) )
					end
				
					renderer.text( "!", verdana_bold, vec2_t.new( pos2d.x - 5, pos2d.y - 75 ), 32, color_t.new( 255, 250, 175, 200 ) )
					renderer.text( tostring( math.round( dist, 0 ) ) .. " ft", verdana_def, vec2_t.new( pos2d.x - 13 - correct_shit, pos2d.y - 43 ), 13, color_t.new( 255, 255, 255, 200 ) )
				end				
			end			
		end		
	end	
end

local function nade_esp( )	
	local m_local = entitylist.get_local_player( )
	local local_origin = m_local:get_prop_vector( origin )
	local local_view = m_local:get_prop_vector( vec_view )
	local _pos = vec3_t.new( local_origin.x + local_view.x, local_origin.y + local_view.y, local_origin.z + local_view.z )
	
	if m_local then
			call_he( _pos )
			call_molotov_fire( _pos )
			call_molotov( _pos )	
	end	
end

client.register_callback( "paint", nade_esp )


-- HeartMarker
local ui_add_checkbox = ui.add_check_box

local is_enabled = ui_add_checkbox('Heartmarkers', 'vis_heartmarkers_enable', false)
local is_colored = ui_add_checkbox('Colored', 'vis_heartmarkers_colored', false)

local hearts = {}

local function rectangle(x, y, w, h, color)
    renderer.rect_filled(vec2_t.new(x, y), vec2_t.new(x + w, y + h), color_t.new(color[1], color[2], color[3], color[4]))
end

-- wtf
local function draw_heart(x, y, color)
    rectangle(x + 2, y + 14, 2, 2, { 0, 0, 0, color[4] })
    rectangle(x, y + 12, 2, 2, { 0, 0, 0, color[4] })
    rectangle(x - 2, y + 10, 2, 2, { 0, 0, 0, color[4] })
    rectangle(x - 4, y + 4, 2, 6, { 0, 0, 0, color[4] })
    rectangle(x - 2, y + 2, 2, 2, { 0, 0, 0, color[4] })
    rectangle(x, y, 2, 2, { 0, 0, 0, color[4] })
    rectangle(x + 2, y, 2, 2, { 0, 0, 0, color[4] })
    rectangle(x + 4, y + 2, 2, 2, { 0, 0, 0, color[4] })
    rectangle(x + 6, y, 2, 2, { 0, 0, 0, color[4] })
    rectangle(x + 8, y, 2, 2, { 0, 0, 0, color[4] })
    rectangle(x + 10, y + 2, 2, 2, { 0, 0, 0, color[4] })
    rectangle(x + 12, y + 4, 2, 6, { 0, 0, 0, color[4] })
    rectangle(x + 10, y + 10, 2, 2, { 0, 0, 0, color[4] })
    rectangle(x + 8, y + 12, 2, 2, { 0, 0, 0, color[4] })
    rectangle(x + 6, y + 14, 2, 2, { 0, 0, 0, color[4] })
    rectangle(x + 4, y + 16, 2, 2, { 0, 0, 0, color[4] })
    rectangle(x - 2, y + 4, 2, 6, { color[1], color[2], color[3], color[4] })
    rectangle(x, y + 2, 4, 2, { color[1], color[2], color[3], color[4] })
    rectangle(x, y + 6, 4, 6, { color[1], color[2], color[3], color[4] })
    rectangle(x + 2, y + 4, 2, 2, { color[1], color[2], color[3], color[4] })
    rectangle(x + 2, y + 12, 2, 2, { color[1], color[2], color[3], color[4] })
    rectangle(x + 4, y + 4, 2, 12, { color[1], color[2], color[3], color[4] })
    rectangle(x + 6, y + 2, 4, 10, { color[1], color[2], color[3], color[4] })
    rectangle(x + 6, y + 12, 2, 2, { color[1], color[2], color[3], color[4] })
    rectangle(x + 10, y + 4, 2, 6, { color[1], color[2], color[3], color[4] })

    rectangle(x, y + 4, 2, 2, { 254, 199, 199, color[4] })
end

local function on_render()
    if not is_enabled:get_value() then return end

    local realtime = globalvars.get_real_time()
    local colored = is_colored:get_value()

    for i = 1, #hearts do
        if hearts[i] == nil then return end
        local heart = hearts[i]

        local vec = se.world_to_screen(
            vec3_t.new(heart.position.x, heart.position.y, heart.position.z)
        )

        local x = vec.x
        local y = vec.y

        local alpha = math.floor(255 - 255 * (realtime - heart.start_time))

        if realtime - heart.start_time >= 1 then
            alpha = 0
        end

        if x ~= nil and y ~= nil then
            if colored then
                if heart.damage <= 15 then
                    draw_heart(x - 5, y - 5, { 60, 255, 0, alpha })
                elseif heart.damage <= 30 then
                    draw_heart(x - 5, y - 5, { 255, 251, 0, alpha })
                elseif heart.damage <= 60 then
                    draw_heart(x - 5, y - 5, { 255, 140, 0, alpha })
                else
                    draw_heart(x - 5, y - 5, { 254, 19, 19, alpha })
                end
            else
                draw_heart(x - 5, y - 5, { 254, 19, 19, 255 })
            end
        end

        heart.position.z = heart.position.z + (realtime - heart.frame_time) * 50
        heart.frame_time = realtime

        if realtime - heart.start_time >= 1 then
            table.remove(hearts, i)
        end
    end
end

local function on_shot(e)
    if e.result ~= 'hit' then return end

    local time = globalvars.get_real_time()

    table.insert(hearts, {
        position = { x = e.aim_point.x, y = e.aim_point.y, z = e.aim_point.z },
        damage = e.server_damage,
        start_time = time,
        frame_time = time
    })
end

client.register_callback('paint', on_render)
client.register_callback('shot_fired', on_shot)


-- JumpScout v1
local astron = {}
astron.jumpscout = ui.add_check_box('Jumpscout', 'jumpscout', false)
astron.jumpscout_hc = ui.add_slider_int('Hitchance in air', 'jumpscout_hc', 0, 100, 35)
astron.jumpscout_da = ui.add_check_box('Disable auto stop', 'jumpscout_da', false)
astron.rage_scout_hitchance = ui.get_slider_int('rage_scout_hitchance')
astron.rage_auto_stop = ui.get_check_box('rage_scout_autostop')
astron.rage_as = ui.get_check_box('rage_scout_autostop'):get_value()
astron.hc_val = ui.get_slider_int('rage_scout_hitchance'):get_value()
astron.flags = se.get_netvar("DT_BasePlayer", "m_fFlags")
function astron.setVisible()
    astron.jumpscout_hc:set_visible(astron.jumpscout:get_value())
    astron.jumpscout_da:set_visible(astron.jumpscout:get_value())
end
function astron.onground(e)
    return bit32.band(e:get_prop_int(astron.flags), 1) == 1
end
function astron.doWork()
    if not astron.jumpscout:get_value() then return end

    if not astron.onground(entitylist.get_local_player()) then
        astron.rage_scout_hitchance:set_value(astron.jumpscout_hc:get_value())
        
        if astron.jumpscout_da:get_value() then
            astron.rage_auto_stop:set_value(false)
        end
    else
        if astron.rage_scout_hitchance:get_value() ~= astron.jumpscout_hc:get_value() then
            astron.hc_val = astron.rage_scout_hitchance:get_value()
        end
        if astron.rage_auto_stop:get_value() ~= false and astron.jumpscout_da:get_value() then
            astron.rage_as = astron.rage_auto_stop:get_value()
        end
        astron.rage_scout_hitchance:set_value(astron.hc_val)
        if astron.jumpscout_da:get_value() then
            astron.rage_auto_stop:set_value(astron.rage_as)
        end
    end
end
function astron.onUnload()
    if not astron.jumpscout:get_value() then return end
    astron.rage_scout_hitchance:set_value(astron.hc_val)
    if astron.jumpscout_da:get_value() then
        astron.rage_auto_stop:set_value(astron.rage_as)
    end
end

client.register_callback("paint", astron.setVisible)
client.register_callback("create_move", astron.doWork)
client.register_callback("unload", astron.onUnload)


-- JumpScout v2
local function main()

    if engine.is_connected() then

        local localPlayer = entitylist.get_local_player()
            
        m_vecVelocity = {
            [0] = se.get_netvar("DT_BasePlayer", "m_vecVelocity[0]"),
            [1] = se.get_netvar("DT_BasePlayer", "m_vecVelocity[1]")
        }
    
        velocity = math.sqrt(localPlayer:get_prop_float(m_vecVelocity[0]) ^ 2 + localPlayer:get_prop_float(m_vecVelocity[1]) ^ 2)

        if velocity ~= nil then
            if velocity > 5 then
                ui.get_check_box("misc_autostrafer"):set_value(true)
            else
                ui.get_check_box("misc_autostrafer"):set_value(false)
            end
        else
            ui.get_check_box("misc_autostrafer"):set_value(true)
        end

    else

        velocity = nil

    end

end

local function on_unload()

    ui.get_check_box("misc_autostrafer"):set_value(true)

end

client.register_callback("paint", main)
client.register_callback("unload", on_unload)


-- Velocity
local screenzxcaa = engine.get_screen_size()
local verdanafontt = renderer.setup_font("C:/windows/fonts/verdana.ttf", 15, 32)
local vosc = renderer.setup_font("C:/windows/fonts/verdana.ttf", 64, 16)
local netvarvel = se.get_netvar("DT_CSPlayer", "m_flVelocityModifier")
local helnetwar = se.get_netvar("DT_BasePlayer", "m_iHealth")
local zxczxcss = { vec2_t.new(screenzxcaa.x / 2 - 45, screenzxcaa.y / 2 - 175), vec2_t.new(screenzxcaa.x / 2 - 15, screenzxcaa.y / 2 - 122), vec2_t.new(screenzxcaa.x / 2 - 72, screenzxcaa.y / 2 - 122) }
local pointsqwe = { vec2_t.new(screenzxcaa.x / 2 - 45, screenzxcaa.y / 2 - 171), vec2_t.new(screenzxcaa.x / 2 - 18, screenzxcaa.y / 2 - 124), vec2_t.new(screenzxcaa.x / 2 - 69, screenzxcaa.y / 2 - 124) }
local bool = false

local function player_hurt(event)
local  localplayerqaz = engine.get_player_info(entitylist.get_local_player():get_index()).name
local  useridqaz = engine.get_player_info(engine.get_player_for_user_id(event:get_int('userid', 0))).name
local  attackerqaz = event:get_int('attacker', 0) ~= 0 and engine.get_player_info(engine.get_player_for_user_id(event:get_int('attacker', 0))).name or 'world'

    if useridqaz == localplayerqaz and attackerqaz ~= localplayerqaz then      
      bool = true 	
    else
      bool = false
    end
end
client.register_callback('player_hurt', player_hurt)

local function on_paint()
local velocity1 = entitylist.get_local_player():get_prop_float(netvarvel)
  velspeed = math.ceil(velocity1 * 100) 
 
  if bool == true then
   visible = 255
  end

  if velspeed > 99 then
   visible = 0
  end
end
client.register_callback("paint", on_paint)

local function on_paint()
     colrspeed = math.min(velspeed*3, 255)
	  zalpok = 255
	 if velspeed*3 >= 200 and velspeed*3 < 210 then
	  zalpok = 240
	 end
	 if velspeed*3 >= 210 and velspeed*3 < 220 then
	  zalpok = 220
	 end
	 if velspeed*3 >= 220 and velspeed*3 < 230 then
	  zalpok = 200
	 end
	 if velspeed*3 >= 230 and velspeed*3 < 240 then
	  zalpok = 180
	 end
	 if velspeed*3 >= 240 and velspeed*3 < 250 then
	  zalpok = 160                                        
	 end
	 if velspeed*3 >= 250 and velspeed*3 < 260 then
	  zalpok = 140
	 end
	 if velspeed*3 >= 260 and velspeed*3 < 270 then
	  zalpok = 120
	 end
	 if velspeed*3 >= 270 and velspeed*3 < 280 then
	  zalpok = 100
	 end
	 if velspeed*3 >= 280 and velspeed*3 < 290 then
	  zalpok = 80
	 end
	 if velspeed*3 >= 290 and velspeed*3 < 300 then
	  zalpok = 60
	 end
	 if velspeed*3 >= 300 then
	  zalpok = 40
	 end
end
client.register_callback("paint", on_paint)

local function on_paint()
local hp123 = entitylist.get_local_player():get_prop_int(helnetwar)
  if hp123 == 0 then return end
	renderer.rect_filled(vec2_t.new(screenzxcaa.x / 2 - 11, screenzxcaa.y / 2 - 132), vec2_t.new(screenzxcaa.x / 2 + 101, screenzxcaa.y / 2 - 151), color_t.new(0 , 0, 0, visible))
	renderer.rect_filled(vec2_t.new(screenzxcaa.x / 2 - 10, screenzxcaa.y / 2 - 133), vec2_t.new(screenzxcaa.x / 2 + velspeed, screenzxcaa.y / 2 - 150), color_t.new(zalpok , colrspeed, 0, visible))
	renderer.filled_polygon(zxczxcss, color_t.new(0, 0, 0, visible))
    renderer.filled_polygon(pointsqwe, color_t.new(zalpok , colrspeed, 0, visible))
	renderer.text("!", vosc, vec2_t.new(screenzxcaa.x / 2 - 51, screenzxcaa.y / 2 - 166), 40, color_t.new(0 , 0, 0, visible))
	renderer.text("Velocity: " .. velspeed .. "%", verdanafontt, vec2_t.new(screenzxcaa.x / 2 - 10, screenzxcaa.y / 2 - 170), 15, color_t.new(0 , 0, 0, visible))
    renderer.text("Velocity: " .. velspeed .. "%", verdanafontt, vec2_t.new(screenzxcaa.x / 2 - 10, screenzxcaa.y / 2 - 169), 15, color_t.new(240 , 240, 240, visible))
end
client.register_callback("paint", on_paint)


-- Mask Change
local ffi = require("ffi")
local bit = require("bit")
 
local __thiscall = function(func, this)
    return function(...) return func(this, ...) end
end
local interface_ptr = ffi.typeof("void***")


local vtable_bind = function(module, interface, index, typedef)
    local addr = ffi.cast("void***", se.create_interface(module, interface)) or error(interface .. " was not found")
    return __thiscall(ffi.cast(typedef, addr[0][index]), addr)
end


local vtable_entry = function(instance, i, ct)
    return ffi.cast(ct, ffi.cast(interface_ptr, instance)[0][i])
end


local vtable_thunk = function(i, ct)
    local t = ffi.typeof(ct)
    return function(instance, ...)
        return vtable_entry(instance, i, t)(instance, ...)
    end
end



local get_class_name = vtable_thunk(143, "const char*(__thiscall*)(void*)")

local set_model_index = vtable_thunk(75, "void(__thiscall*)(void*,int)")


local get_client_entity_from_handle = vtable_bind("client.dll", "VClientEntityList003", 4, "void*(__thiscall*)(void*,void*)")

local get_model_index = vtable_bind("engine.dll", "VModelInfoClient004", 2, "int(__thiscall*)(void*, const char*)")


local rawientitylist = se.create_interface('client.dll', 'VClientEntityList003') or error('VClientEntityList003 was not found', 2)
local ientitylist = ffi.cast(interface_ptr, rawientitylist) or error('rawientitylist is nil', 2)

local get_client_entity = ffi.cast('void*(__thiscall*)(void*, int)', ientitylist[0][3]) or error('get_client_entity was not found', 2)


local client_string_table_container = ffi.cast(interface_ptr, se.create_interface('engine.dll', 'VEngineClientStringTable001')) or error('VEngineClientStringTable001 was not found', 2)
local find_table = vtable_thunk(3, 'void*(__thiscall*)(void*, const char*)')


local model_info = ffi.cast(interface_ptr, se.create_interface('engine.dll', 'VModelInfoClient004')) or error('VModelInfoClient004 wasnt found', 2)


ffi.cdef [[
    typedef void(__thiscall* find_or_load_model_t)(void*, const char*);
]]


local add_string = vtable_thunk(8, "int*(__thiscall*)(void*, bool, const char*, int length, const void* userdata)")

local find_or_load_model = ffi.cast("find_or_load_model_t", model_info[0][43]) -- vtable thunk crashes (?)

local function _precache(szModelName)
    if szModelName == "" then return end -- don't precache empty strings (crash)
    if szModelName == nil then return end
    szModelName = string.gsub(szModelName, [[\]], [[/]])

    local m_pModelPrecacheTable = find_table(client_string_table_container, "modelprecache")
    if m_pModelPrecacheTable ~= nil then
        find_or_load_model(model_info, szModelName)
        add_string(m_pModelPrecacheTable, false, szModelName, -1, nil)
    end
end


local changer = {}


changer.list_names = {'None', 'Dallas', 'Battle Mask', 'Evil Clown', 'Anaglyph', 'Boar', 'Bunny', 'Bunny Gold', 'Chains', 'Chicken', 'Devil Plastic', 'Hoxton', 'Pumpkin', 'Samurai', 'Sheep Bloody', 'Sheep Gold', 'Sheep Model', 'Skull', 'Template', 'Wolf', 'Doll',}
changer.models = {'', 'models/player/holiday/facemasks/facemask_dallas.mdl', 'models/player/holiday/facemasks/facemask_battlemask.mdl', 'models/player/holiday/facemasks/evil_clown.mdl', 'models/player/holiday/facemasks/facemask_anaglyph.mdl', 'models/player/holiday/facemasks/facemask_boar.mdl', 'models/player/holiday/facemasks/facemask_bunny.mdl', 'models/player/holiday/facemasks/facemask_bunny_gold.mdl', 'models/player/holiday/facemasks/facemask_chains.mdl', 'models/player/holiday/facemasks/facemask_chicken.mdl', 'models/player/holiday/facemasks/facemask_devil_plastic.mdl', 'models/player/holiday/facemasks/facemask_hoxton.mdl', 'models/player/holiday/facemasks/facemask_pumpkin.mdl', 'models/player/holiday/facemasks/facemask_samurai.mdl', 'models/player/holiday/facemasks/facemask_sheep_bloody.mdl', 'models/player/holiday/facemasks/facemask_sheep_gold.mdl', 'models/player/holiday/facemasks/facemask_sheep_model.mdl', 'models/player/holiday/facemasks/facemask_skull.mdl', 'models/player/holiday/facemasks/facemask_template.mdl', 'models/player/holiday/facemasks/facemask_wolf.mdl', 'models/player/holiday/facemasks/porcelain_doll.mdl',}

changer.last_model = 0
changer.model_index = -1
changer.enabled = false


local combobox_masks = ui.add_combo_box("Current mask", "lua_mask", changer.list_names, 0)


local function precache(modelPath)
    if modelPath == "" then return -1 end -- don't crash.
    local local_model_index = get_model_index(modelPath)
    if local_model_index == -1 then
        _precache(modelPath)
    end
    return get_model_index(modelPath)
end

local function on_paint()
    if not engine.is_in_game() then
        changer.last_model = 0
        return
    end
    if changer.last_model ~= combobox_masks:get_value() then
        changer.last_model = combobox_masks:get_value()
        if changer.last_model == 0 then
            changer.enabled = false
        else
            changer.enabled = true
            changer.model_index = precache(changer.models[changer.last_model + 1])
        end
    end
end


local function get_player_address(lp)
    return get_client_entity(ientitylist, lp:get_index())
end


local function on_setup_command(cmd)
    if changer.model_index == -1 then return precache(changer.models[changer.last_model + 1]) end

    local local_player = entitylist.get_local_player()
    if changer.enabled then
        local lp_addr = ffi.cast("intptr_t*", get_player_address(local_player))
        local m_AddonModelsHead = ffi.cast("intptr_t*", lp_addr + 0x462F) -- E8 ? ? ? ? A1 ? ? ? ? 8B CE 8B 40 10
        local i, next_model = m_AddonModelsHead[0], -1

        while i ~= -1 do
            next_model = ffi.cast("intptr_t*", lp_addr + 0x462C)[0] + 0x18 * i -- this is the pModel (CAddonModel) afaik
            i = ffi.cast("intptr_t*", next_model + 0x14)[0]
            local m_pEnt = ffi.cast("intptr_t**", next_model)[0] -- CHandle<C_BaseAnimating> m_hEnt -> Get()
            local m_iAddon = ffi.cast("intptr_t*", next_model + 0x4)[0]
            if tonumber(m_iAddon) == 16 then -- face mask addon bits knife = 10
                local entity = get_client_entity_from_handle(m_pEnt)
                set_model_index(entity, changer.model_index)
            end
        end
    end
end


local m_iAddonBits = se.get_netvar("DT_CSPlayer", "m_iAddonBits")

client.register_callback("create_move", function()
    local local_player = entitylist.get_local_player()
    if local_player == nil then return end
    if changer.enabled then
        if bit.band(local_player:get_prop_int(m_iAddonBits), 0x10000) ~= 0x10000 then
            local_player:set_prop_int(m_iAddonBits, 0x10000 + local_player:get_prop_int(m_iAddonBits))
        end
    else
        if bit.band(local_player:get_prop_int(m_iAddonBits), 0x10000) == 0x10000 then
            local_player:set_prop_int(m_iAddonBits, local_player:get_prop_int(m_iAddonBits) - 0x10000)
        end
    end
end)

client.register_callback('paint', on_paint)
client.register_callback("create_move", on_setup_command)


-- Trash Talk
local advertisement = {
    "соуфив лох",

}

local messages = {
	"на нахуй",

}

--local trashtalk_enabled = ui.add_check_box("Enable Trashtalk", "trashtalk_enabled", false)
local trashtalk_type = ui.add_combo_box("Trashtalk type", "trashtalk_type", { "Off", "TrashTalk", "Advertisement" }, 0)


client.register_callback("player_death", function(event)
    
    local attacker_index = engine.get_player_for_user_id(event:get_int("attacker",0))
    local died_index = engine.get_player_for_user_id(event:get_int("userid",1))
    local me = engine.get_local_player()
    
    math.randomseed(os.clock()*100000000000)

        if attacker_index == me and died_index ~= me then      
            if trashtalk_type:get_value() == 1 then    
                engine.execute_client_cmd("say" .. tostring(messages[math.random(0, #messages)]))
            elseif trashtalk_type:get_value() == 2 then
                engine.execute_client_cmd("say" .. tostring(advertisement[math.random(0, #advertisement)]))
            end
        end
end)
